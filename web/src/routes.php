<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view

    $stmt = $this->db->query('SELECT count(*) as pocet, date(date) as datum FROM zaznamy WHERE akce=\'otevrit\' GROUP BY datum ORDER BY datum');
    $tplVars['statistika'] = $stmt->fetchALL();
    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');

$app->get('/zaznamy', function (Request $request, Response $response, $args) {
    // Render index view
    $stmt = $this->db->query('SELECT * FROM zaznamy');
    $tplVars['zaznamy'] = $stmt->fetchALL();
    return $this->view->render($response, 'zaznamy.latte',$tplVars);
})->setName('zaznamy');



//route otevrit
$app->post('/otevrit', function (Request $request, Response $response, $args) {

        try {
            $stmt = $this->db->prepare("INSERT INTO zaznamy
                           (image, akce)
                           VALUES ('placeholder.jpg','otevrit')");
            $stmt->execute();
            //redirect zpet na index
            return $response->withHeader('Location', $this->router->pathFor('index'));

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }
})->setName('otevrit');


//route zavrit
$app->post('/zavrit', function (Request $request, Response $response, $args) {

    try {
        $stmt = $this->db->prepare("INSERT INTO zaznamy
                           (image, akce)
                           VALUES ('placeholder.jpg','zavrit')");
        $stmt->execute();
        //redirect zpet na index
        return $response->withHeader('Location', $this->router->pathFor('index'));

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
})->setName('zavrit');


//route zamcit
$app->post('/zamcit', function (Request $request, Response $response, $args) {

    try {
        $stmt = $this->db->prepare("INSERT INTO zaznamy
                           (image, akce)
                           VALUES ('placeholder.jpg','zamcit')");
        $stmt->execute();
        //redirect zpet na index
        return $response->withHeader('Location', $this->router->pathFor('index'));

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
})->setName('zamcit');

$app->get('/API/zaznamy', function (Request $request, Response $response, $args) {
    // Render index view
    $stmt = $this->db->query('SELECT count(*) as pocet, date(date) as datum FROM zaznamy GROUP BY datum ORDER BY datum');
    $payload = $stmt->fetchALL(PDO::FETCH_ASSOC);
    $response->write(json_encode($payload));
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(201);
})->setName('api');